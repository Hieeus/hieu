$(document).ready(function () {
    $('#main-slider').owlCarousel({
        loop: true,
        margin: 0,
        autoplay: true,
        interval: 7000,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    })
    $('#news-food-slider').owlCarousel({
        loop: false,
        rewind: true,
        margin: 15,
        nav: true,
        dots: false,
        autoplay: true,
        interval: 4000,
        responsive: {
            0: {
                items: 1
            },
            400: {
                items: 2
            },
            600: {
                items: 2
            },
            1000: {
                items: 4
            }
        }
    })

});
$(document).ready(function () {
    $(".icon-respon-menu").click(function () {
        $("#site").toggleClass('open-respon-menu');
        $(".icon-respon-menu .fas").toggleClass('fa-bars fa-times');
        return false;
    });
    $(window).resize(function () {
        if ($(window).width() > 768) {
            $("#site").removeClass('open-respon-menu');
            $(".icon-respon-menu .fas").removeClass('fa-times').addClass('fa-bars');
        }
    });
    $(window).scroll(function () {
        if ($(window).scrollTop() > 0) {
            $("#site").removeClass('open-respon-menu');
            $(".icon-respon-menu .fas").removeClass('fa-times').addClass('fa-bars');
        }
    });

    $(window).scroll(function () {
        if ($(window).scrollTop() > 0) {
            $("#back-to-top").slideDown(500)
        } else {
            $("#back-to-top").slideUp(500);
        }
    });
    $("#back-to-top").click(function () {
        $("body, html").animate(
            {
                scrollTop: 0
            }, 600
        );
    });

    // var prevScrollpos = $(window).scrollTop();
    // $(window).scroll(function () {
    //     var currentScrollPos = $(window).scrollTop();
    //     if (prevScrollpos > currentScrollPos) {
    //         $("#header").css('top', '0px');
    //     } else {
    //         $("#header").css('top', '-100px');
    //     }
    //     prevScrollpos = currentScrollPos;
    // });

});